
;;; my-doom-one-light-theme.el --- Opera-Light theme -*- no-byte-compile: t; -*-

(require 'doom-themes)

(defgroup my-doom-one-light-theme nil
  "Options for doom-themes"
  :group 'doom-themes)

(defcustom my-doom-one-light-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'my-doom-one-light-theme
  :type 'boolean)

(defcustom my-doom-one-light-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'my-doom-one-light-theme
  :type 'boolean)

(defcustom my-doom-one-light-comment-bg my-doom-one-light-brighter-comments
  "If non-nil, comments will have a subtle, darker background. Enhancing their
legibility."
  :group 'my-doom-one-light-theme
  :type 'boolean)

(defcustom my-doom-one-light-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line. Can be an integer to
determine the exact padding."
  :group 'my-doom-one-light-theme
  :type '(choice integer boolean))

(defcustom my-doom-one-light-region-highlight t
  "Determines the selection highlight style. Can be 'frost, 'snowstorm or t
(default)."
  :group 'my-doom-one-light-theme
  :type 'symbol)

(def-doom-theme my-doom-one-light
  "A light Opera theme."

  ;; name        default   256       16
  ((bg         '("#FFFFFF" nil       nil ))
   (bg-alt     '("#FFFFFF" nil       nil ))
   (base0      '("#fafafa" "#dfdfdf" nil ))
   (base1      '("#f5f5f5" "#979797" nil ))
   (base2      '("#eeeeee" "#6b6b6b" nil ))
   (base3      '("#e0e0e0" "#525252" nil ))
   (base4      '("#bdbdbd" "#3f3f3f" nil ))
   (base5      '("#9e9e9e" "#262626" nil ))
   (base6      '("#757575" "#2e2e2e" nil ))
   (base7      '("#616161" "#1e1e1e" nil ))
   (base8      '("#424242" "black"   nil ))
   (fg         '("#2a2a2a" "#2a2a2a" nil ))
   (fg-alt     '("#454545" "#757575" nil ))

   (grey       base4)
   (red        '("#EF5350" "#ff6655" nil ))
   (orange     '("#FFA726" "#dd8844" nil ))
   (green      '("#66BB6A" "#99bb66" nil ))
   (teal       '("#26A69A" "#44b9b1" nil ))
   (yellow     '("#FFA726" "#ECBE7B" nil ))
   (blue       '("#42A5F5" "#51afef" nil ))
   (dark-blue  '("#1A237E" "#2257A0" nil ))
   (magenta    '("#AB47BC" "#c678dd" nil ))
   (violet     '("#7E57C2" "#a9a1e1" nil ))
   (cyan       '("#26C6DA" "#46D9FF" nil ))
   (dark-cyan  '("#006064" "#5699AF" nil ))

   ;; face categories -- required for all themes
   (highlight      blue)
   (vertical-bar   bg)
   (selection      dark-blue)
   (builtin        teal)
   (comments       base3)
   (doc-comments   base3)
   (constants      magenta)
   (functions      teal)
   (keywords       blue)
   (methods        teal)
   (operators      blue)
   (type           cyan)
   (strings        green)
   (variables      (doom-lighten magenta 0.5))
   (numbers        magenta)
   (region         base2)
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    orange)
   (vc-added       green)
   (vc-deleted     red)
   (shadow         base1)

   ;; custom categories
   (hidden     `(,(car bg) "black" "black"))

  ;; --- extra faces ------------------------
  (
   ((lazy-highlight &override) :foreground base1 :weight 'bold)
   ((line-number &override) :foreground fg-alt)
   ((line-number-current-line &override) :foreground fg)

   (font-lock-comment-face
    :foreground comments
    :background (if my-doom-one-light-comment-bg (doom-lighten bg 0.05)))
   (font-lock-doc-face
    :inherit 'font-lock-comment-face
    :foreground doc-comments)

   (doom-modeline-bar :background (if -modeline-bright modeline-bg highlight))

   ;; ivy-posframe
   (ivy-posframe :background bg-alt)
   (ivy-posframe-border :background base1)

   ;; ivy
   (ivy-current-match :background base3)

   (mode-line
    :background bg)

   (org-block
    :background bg)
   (shadow
    :foreground base3)
   (hl-line
    :background base1)
   (bold
    :weight 'ultrabold)
   (org-block-begin-line
    :foreground base3
    :background bg)
   (org-date
    :inherit 'font-lock-comment-face)
   (org-drawer
    :inherit 'font-lock-comment-face)
   (header-line
    :foreground "#2a2a2a"
    :distant-foreground "#FFFFFF"
    :background "#E5E5E5")

   ))

;;; my-doom-one-light-theme.el ends here
